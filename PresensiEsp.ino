#include "Init.h"
long count = 0;

void setup() {
  Serial.begin(115200);
  Serial.println();
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.begin(ssid.c_str(), pass.c_str());

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

void loop() {
  String dataEpoch        = String(epochConverter(2019, 11, 11, 11, 11, 11 + count++));
  int kode_fingerprint    = random(0, 10);
  int id_data_fingerprint = random(0, 101);
  String tipe_kendaraan[] = {"truck", "mobil", "tank", "karpetaladin", "buroq"};
  String nama_driver[]    = {"Imam", "Mamun", "Alwy", "Jokowi", "Bruno", "Antok"};
  String plat_nomor[]     = {"AD4851LO", "N90903NO", "Y8938JJ", "AB90190L", "U9090IO", "T78173LO"};

  //  {{waktu}}/{{kode_fingerprint}}/{{id_data_fingerprint}}/{{tipe_kendaraan}}/{{nama_driver}}/{{plat_nomor}}

  String dataMessage = dataEpoch + "/";
  dataMessage += String(kode_fingerprint) + "/";
  dataMessage += String(id_data_fingerprint) + "/";
  dataMessage += String(tipe_kendaraan[random(0, 6)]) + "/";
  dataMessage += String(nama_driver[random(0, 6)]) + "/";
  dataMessage += String(plat_nomor[random(0, 6)]);

  if(toWebService(dataMessage)){
    Serial.println("Succed");
    Serial.println(dataMessage);
  }
  else{
    Serial.println("Failed");
  }

  //  if (toWebservice(dataEpoch,
  //                   String(kode_fingerprint),
  //                   String(id_data_fingerprint),
  //                   tipe_kendaraan[random(0, 3)],
  //                   nama_driver[random(0, 3)],
  //                   plat_nomor[random(0, 3)])
  //     ) {
  //    Serial.println("Succed...");
  //  } else {
  //    Serial.println("Failed...");
  //  }

  delay(500);
}

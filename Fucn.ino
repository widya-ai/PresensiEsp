time_t epochConverter(int YYYY,
                      int MM,
                      int DD,
                      int hh,
                      int mm,
                      int ss) {
  tmElements_t tmSet;
  tmSet.Year = YYYY - 1970;
  tmSet.Month = MM;
  tmSet.Day = DD;
  tmSet.Hour = hh;
  tmSet.Minute = mm;
  tmSet.Second = ss;
  return makeTime(tmSet);
}

boolean toWebservice(String epoch,
                     String kode_fingerprint,
                     String id_data_fingerprint,
                     String tipe_kendaraan,
                     String nama_driver,
                     String plat_nomor) {

  String getData = "/presensi-driver/log/";

  getData += epoch + "/";               // {{waktu}}
  getData += kode_fingerprint + "/";    // {{kode_fingerprint}}
  getData += id_data_fingerprint + "/"; // {{id_data_fingerprint}}
  getData += tipe_kendaraan + "/";       // {{tipe_kendaraan}}
  getData += nama_driver + "/";         // {{nama_driver}}
  getData += plat_nomor;                // {{plat_nomor}}

  client.get(getData);

  // read the status code and body of the response
  int statusCode = client.responseStatusCode();
  String response = client.responseBody(); // Harus dicantumkan/dipakai, kalo tidak nanti error/status code nya -4

  if (statusCode == 201) {
    return true;
  }
  else {
    return false;
  }
}

boolean toWebService(String dataMessage) {
  dataMessage.replace(",", "/");
  
  String getData = "/presensi-driver/log/";
  
  getData += dataMessage;
  client.get(getData);

  // read the status code and body of the response
  int statusCode = client.responseStatusCode();
  String response = client.responseBody(); // Harus dicantumkan/dipakai, kalo tidak nanti error/status code nya -4

  if (statusCode == 201) {
    return true;
  }
  else {
    return false;
  }
}
